import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'medicalAppointment';
  showItems = false;
  constructor(private authenticationService: AuthenticationService, private router: Router) {

      this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                // Hide loading indicator
               if(event.urlAfterRedirects !== "/login" && event.urlAfterRedirects !== "/register") {
                  this.showItems = true;
                } else {
                  this.showItems = false;
                }
            }
        });

  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

}
