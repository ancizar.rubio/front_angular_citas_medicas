import { UtilsService } from './../services/utils.service';
import { MatSnackBar } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../models/user';
import { RegisterService } from './../services/register.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-patient',
  templateUrl: './dashboard-patient.component.html',
  styleUrls: ['./dashboard-patient.component.scss']
})

export class DashboardPatientComponent implements OnInit {

  user: any;

  userUpdate: FormGroup;
  registroCita: FormGroup;
  updateCita: FormGroup;
  loading = false;
  submitted = false;
  doctores = [];
  typeCites = [];
  citas = [];
  step = 0;

  constructor(
  private userAuth: RegisterService,
  private formBuilder: FormBuilder,
  private _snackbar: MatSnackBar,
  private agenda: UtilsService
  ) { }

  ngOnInit() {
    this.userAuth.getAll().subscribe(user => {
      console.log(user)
      this.user = user;
    }, error => {
      console.log(error)
    })

    this.agenda.getAllDoctors().subscribe(docs => {
      this.doctores.push(docs)
    })

    this.agenda.getAllCiteType().subscribe(citeType => {
      this.typeCites.push(citeType)
    })

    this.getAllCitas();

    this.userUpdate = this.formBuilder.group({
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
    });

    this.registroCita = this.formBuilder.group({
        fecha: ['', Validators.required],
        doctor: ['', Validators.required],
        tipo_cita: ['', Validators.required],
    });

    this.updateCita = this.formBuilder.group({
        fecha: ['', Validators.required],
        doctor: ['', Validators.required]
    })

  }

  setStep(index: number) {
    this.step = index;
  }

  getAllCitas() {
    this.citas = [];
    this.agenda.getAllCitas().subscribe(citas => {
      console.log('entro aquii')
      this.citas.push(citas);
      console.log(this.citas)
    })
  }

  onSubmitUserUpdate() {
    this.submitted = true;
// stop here if form is invalid
    if (this.userUpdate.invalid) {
        return;
    }

    let newUser = {
        'pk': this.user.pk,
        'username': this.user.username,
        'email': this.user.email,
        'first_name': this.userUpdate.get('first_name').value,
        'last_name': this.userUpdate.get('last_name').value
    };

    this.loading = true;
    this.userAuth.userUpdate(newUser).subscribe(
    data => {
        this.submitted = false;
        this.registroCita.reset(null);
    },
    error => {
        this._snackbar.open(error.error.email, 'ok');
        this.loading = false;
        this.submitted = false;
    });

  }

  registrarCita() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registroCita.invalid) {
        return;
    }

    let newCita = [{
        'usuario': this.user.pk,
        'fecha': this.registroCita.get('fecha').value,
        'doctor': this.registroCita.get('doctor').value,
        'cita': this.registroCita.get('tipo_cita').value,
    }];

    this.loading = true;
    this.agenda.registrarCita(newCita).subscribe(
    data => {
        this.submitted = false;
        this.loading = false;
    },
    error => {
        this._snackbar.open('error al crear la cita medica', 'ok');
        this.loading = false;
        this.submitted = false;
    });
  }

  updateCitaReg(idCita) {
    console.log(idCita)
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateCita.invalid) {
        return;
    }

    let newCita = [{
        'usuario': this.user.pk,
        'fecha': this.updateCita.get('fecha').value,
        'doctor': this.updateCita.get('doctor').value,
        'cita': idCita,
    }];

    console.log(newCita)

    this.loading = true;
    this.agenda.updateCita(idCita, newCita).subscribe(
    data => {
        this.submitted = false;
        this.loading = false;
        setTimeout(() => {
          this.getAllCitas()
        }, 2000);

    },
    error => {
        this._snackbar.open('error al actualizar la cita medica', 'ok');
        this.loading = false;
        this.submitted = false;
        setTimeout(() => {
          this.getAllCitas()
        }, 2000);
    });
  }

}
