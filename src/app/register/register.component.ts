import { RegisterService } from './../services/register.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from '../helpers/MustMatch';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  hide = true;
  hideTwo = true;
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private registerService: RegisterService,
        private _snackbar: MatSnackBar) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        correo: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordTwo: ['', [Validators.required, Validators.minLength(6)]]
    }, {
            validator: MustMatch('password', 'passwordTwo')
        });
  }

  onSubmit() {
    this.submitted = true;
// stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    console.log(this.registerForm.get('correo').value);

    let newUser = {
        'email': this.registerForm.get('correo').value,
        'password1': this.registerForm.get('password').value,
        'password2': this.registerForm.get('passwordTwo').value
    };

    this.loading = true;
    this.registerService.register(newUser).subscribe(
    data => {
        this.router.navigate(['login']);
        this.submitted = false;
    },
    error => {
        this._snackbar.open(error.error.email, 'ok');
        this.loading = false;
        this.submitted = false;
    });

  }

}
