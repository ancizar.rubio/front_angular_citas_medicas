import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.API_URL}/auth/user/`);
    }

    userUpdate(user) {
        return this.http.put<User[]>(`${environment.API_URL}/auth/user/`, user);
    }

    register(user: User) {
        return this.http.post(`${environment.API_URL}/auth/registration/`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.API_URL}/auth/${id}`);
    }
}
