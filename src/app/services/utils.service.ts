import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private http: HttpClient) { }

  getAllDoctors() {
      return this.http.get(`${environment.API_URL}/doctores/doctores/`);
  }

  getAllCiteType() {
      return this.http.get(`${environment.API_URL}/doctores/tipo-cita/`);
  }

  getAllCitas() {
      return this.http.get(`${environment.API_URL}/agenda/get-agenda/`);
  }

  registrarCita(citaMedica) {
      return this.http.post(`${environment.API_URL}/agenda/post-agenda/`, citaMedica);
  }

  updateCita(idCita, citaMedica) {
      return this.http.put(`${environment.API_URL}/agenda/updelete/${idCita}/`, citaMedica);
  }

}
